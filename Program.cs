using Azure.Core;
using Azure.Identity;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

// 创建默认Azure身份验证器
var credential = new DefaultAzureCredential();

app.MapGet("/", () => "Hello World!");

app.Run();